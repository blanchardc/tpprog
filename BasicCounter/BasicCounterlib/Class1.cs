﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace compteur
{
    public class Compteur
    {

        static private int compteur;
        


        public ClassCompteur()
        {
            compteur = 0; 
        }

        public int get_compteur() { return nb_compteur;  }
        public void set_compteur(int new_comp) { nb_compteur = new_comp; }
        public void incrementation()
        {
            compteur++; 
        } 
        public void decrementation()
        {
            if (compteur > 0)
                compteur--;
            else
                compteur = 0;
        }
        public void remise_a_zero()
        {
            compteur = 0; 
        }
        
     
    }
}
