﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lb_Total = New System.Windows.Forms.Label()
        Me.lb_compteur = New System.Windows.Forms.Label()
        Me.bp_inc = New System.Windows.Forms.Button()
        Me.bp_dec = New System.Windows.Forms.Button()
        Me.bp_RAZ = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lb_Total
        '
        Me.lb_Total.AutoSize = True
        Me.lb_Total.Location = New System.Drawing.Point(370, 92)
        Me.lb_Total.Name = "lb_Total"
        Me.lb_Total.Size = New System.Drawing.Size(31, 13)
        Me.lb_Total.TabIndex = 0
        Me.lb_Total.Text = "Total"
        '
        'lb_compteur
        '
        Me.lb_compteur.AutoSize = True
        Me.lb_compteur.Location = New System.Drawing.Point(379, 164)
        Me.lb_compteur.Name = "lb_compteur"
        Me.lb_compteur.Size = New System.Drawing.Size(13, 13)
        Me.lb_compteur.TabIndex = 1
        Me.lb_compteur.Text = "0"
        '
        'bp_inc
        '
        Me.bp_inc.Location = New System.Drawing.Point(538, 164)
        Me.bp_inc.Name = "bp_inc"
        Me.bp_inc.Size = New System.Drawing.Size(75, 23)
        Me.bp_inc.TabIndex = 2
        Me.bp_inc.Text = "+"
        Me.bp_inc.UseVisualStyleBackColor = True
        '
        'bp_dec
        '
        Me.bp_dec.Location = New System.Drawing.Point(198, 164)
        Me.bp_dec.Name = "bp_dec"
        Me.bp_dec.Size = New System.Drawing.Size(75, 23)
        Me.bp_dec.TabIndex = 3
        Me.bp_dec.Text = "-"
        Me.bp_dec.UseVisualStyleBackColor = True
        '
        'bp_RAZ
        '
        Me.bp_RAZ.Location = New System.Drawing.Point(343, 271)
        Me.bp_RAZ.Name = "bp_RAZ"
        Me.bp_RAZ.Size = New System.Drawing.Size(75, 23)
        Me.bp_RAZ.TabIndex = 4
        Me.bp_RAZ.Text = "RAZ"
        Me.bp_RAZ.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.bp_RAZ)
        Me.Controls.Add(Me.bp_dec)
        Me.Controls.Add(Me.bp_inc)
        Me.Controls.Add(Me.lb_compteur)
        Me.Controls.Add(Me.lb_Total)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lb_Total As Label
    Friend WithEvents lb_compteur As Label
    Friend WithEvents bp_inc As Button
    Friend WithEvents bp_dec As Button
    Friend WithEvents bp_RAZ As Button
End Class
