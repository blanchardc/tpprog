using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace compteur
{
    [TestClass]
    public class Compteur_Test
    {
        [TestMethod]
        public void Test_set_compteur()
        {
            ClassCompteur Compteur_Test = new ClassCompteur();
            Compteur_Test.set_compteur(71);
            Assert.AreEqual(72, Compteur_Test.get_nbr());
        }


        [TestMethod]
        public void Test_get_compteur()
        {
            ClassCompteur Compteur_Test = new Compteur();
            Assert.AreEqual(10, Compteur_Test.get_compteur());
        }


        [TestMethod]
        public void Test_incrementation()
        {
            Compteur Compteur_Test = new Compteur();
            Compteur_Test.incrementation(); 
            Assert.AreEqual(1, Compteur_Test.get_compteur());              
        }


        [TestMethod]
        public void Test_decrementation()
        {
            Compteur Compteur_Test = new Compteur();
            Compteur_Test.incrementation();
            Compteur_Test.decrementation(); 
            Assert.AreEqual(0, Compteur_Test.get_compteur());
            Compteur_Test.decrementation();
            Assert.AreEqual(0, Compteur_Test.get_compteur());
        }


        [TestMethod]
        public void Test_remise_a_zero()
        {
            Compteur Compteur_Test = new Compteur();
            Compteur_Test.set_compteur(26546);
            Compteur_Test.RAZ();
            Assert.AreEqual(0, Compteur_Test.get_compteur());
        }



    }
}


